#Install java
sudo yum install java-1.8.0-openjdk-devel
#Enable Jenkins repository
curl --silent --location http://pkg.jenkins-ci.org/redhat-stable/jenkins.repo | sudo tee /etc/yum.repos.d/jenkins.repo
#Add your repository with Key
sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key
#Install Jenkins
sudo yum install jenkins
#Start Jenkins
sudo systemctl start jenkins
#Status check Jenkins
systemctl status jenkins
#Enable Jenkins daemon service
sudo systemctl enable jenkins
#Adjust firewall settings in the Local machine
sudo firewall-cmd --permanent --zone=public --add-port=8080/tcp
sudo firewall-cmd --reload
